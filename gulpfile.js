var gulp = require('gulp');
var replace = require('gulp-replace');
var vulcanize = require('gulp-vulcanize');
var crisper = require('gulp-crisper');
var connect = require('gulp-connect');
var shell = require('gulp-shell');
var minifyInline = require('gulp-minify-inline');

gulp.task('build', function() {
    gulp.src('temp/6D8670D6-B82B-4A6B-9C96-5A71F8D6FD53/*')
        .pipe(gulp.dest('dist/6D8670D6-B82B-4A6B-9C96-5A71F8D6FD53'));

    gulp.src('campus-homework-for-teacher.html')
        .pipe(replace(/<link rel="import" href="../g, '<link rel="import" href="../../bower_components'))
        .pipe(gulp.dest('dist/6D8670D6-B82B-4A6B-9C96-5A71F8D6FD53'))
        .pipe(replace(/<script src="../g, '<script src="../../bower_components'))
        .pipe(gulp.dest('dist/6D8670D6-B82B-4A6B-9C96-5A71F8D6FD53'))
        .pipe(replace(/campus-behavior.html/g, 'campus-develope.html'))
        .pipe(gulp.dest('dist/6D8670D6-B82B-4A6B-9C96-5A71F8D6FD53'))
        .pipe(vulcanize({
            inlineScripts: true,
            inlineCss: true,
            stripComments: true
        }))
        .pipe(minifyInline())
        .pipe(crisper())
        .pipe(gulp.dest('dist/6D8670D6-B82B-4A6B-9C96-5A71F8D6FD53'));

    gulp.src('temp/75549BBD-5A71-447C-AC12-DA258F6C67CF/*')
        .pipe(gulp.dest('dist/75549BBD-5A71-447C-AC12-DA258F6C67CF'));

    return gulp.src('campus-homework-for-student.html')
        .pipe(replace(/<link rel="import" href="../g, '<link rel="import" href="../../bower_components'))
        .pipe(gulp.dest('dist/75549BBD-5A71-447C-AC12-DA258F6C67CF'))
        .pipe(replace(/<script src="../g, '<script src="../../bower_components'))
        .pipe(gulp.dest('dist/75549BBD-5A71-447C-AC12-DA258F6C67CF'))
        .pipe(replace(/campus-behavior.html/g, 'campus-develope.html'))
        .pipe(gulp.dest('dist/75549BBD-5A71-447C-AC12-DA258F6C67CF'))
        .pipe(vulcanize({
            inlineScripts: true,
            inlineCss: true,
            stripComments: true
        }))
        .pipe(minifyInline())
        .pipe(crisper())
        .pipe(gulp.dest('dist/75549BBD-5A71-447C-AC12-DA258F6C67CF'));
});

gulp.task('serve', function() {
    connect.server({
        root: 'dist'
    });
});

gulp.task('default', ['serve', 'build'], shell.task([
    /^win/.test(require('os').platform()) ? 'start http://localhost:8080/' : 'open http://localhost:8080/'
]));
